# PBOX - Per accedere dall'esterno alla vostra local area network  - in sicurezza!!

<u>PBOX sta per **P**rivate **BOX**, ovvero un dispositivo che vi permette di accedere alla vostra rete locale ovunque siate in completa sicurezza.</u>

L'accesso avviene in **sicurezza** poiché PBOX funge da controllore di tutto il traffico che transita da internet affinché questo provenga da un computer autorizzato all'accesso provvedendo alla crittazione di tutte le comunicazioni da e verso questo.

PBOX è infatti un **Virtual Private Network (VPN) server**, e potete pensarlo come una presa ethernet virtuale per un computer o dispositivo che non si trova fisicamente sulla vostra rete ma è connesso ad internet in da qualunque altra postazione nel mondo.

PBOX è assolutamente **invisibile dall'esterno**, e la sua presenza non è in alcun modo individuabile da internet. E' dotato di un semplice **pannello di controllo** in grado di **generare o revocare i certificati** di accesso per i client.

Il software di PBOX è **opensource**, e si installa su qualunque computer Raspberry PI di tipo 3 o successivi, il cui costo è minimo (attualmente sotto i 100 euro).

![PBOX](images/pbox.jpg)

### Cosa vi serve

##### Requisiti della rete locale

- Una connessione ad internet nella rete locale che volete sia raggiungibile dall'esterno, possibilmente a banda larga FTT (fibra)
- modem/router o x-router con cui effettuate l'accesso ad internet di cui conosciate i parametri di accesso per la configurazione (port forwarding, DNS dinamico, DHCP server con prenotazione DHCP)

##### Requisiti hardware

- Computer Raspberry Pi (3B+ [--link amazon--](https://amzn.to/3blx4Lw) o 4 [--link amazon--](https://amzn.to/2Wg6DT4)) completo di case e alimentatore, e con una microSD scard da almeno 16GB
- un cavo ethernet per collegare la PBOX al router

##### Requisiti hardware/software del computer con cui sarà inizializzata la PBOX

- Il computer deve possedere uno slot o accessorio USB per microSD [--link amazon--](https://amzn.to/2Agh5Sl)
- Un software per la scrittura di una immagine linux su SD card (es: [Balena Etcher](https://www.balena.io/etcher/ "Balena Etcher"))

##### Requisiti software dei dispositivi che si collegheranno dall'esterno (client)

- Un software client OpenVPN con gestione modalità TAP da installare sui computer che dovranno accedere dall'esterno
  es: [SparkLabs Viscosity (windows/Mac gratuito 30gg, shareware)](https://www.sparklabs.com/viscosity/) - [TunnelBlick versione Mac (freeware)](https://tunnelblick.net) - [SecurePoint SSL versione Windows (freeware)](https://sourceforge.net/projects/securepoint/)

### Come funziona

PBOX utilizza il sistema operativo Raspbian buster *(non è compatibile con i precedenti)* con installato il pacchetto [OpenVPN server](https://openvpn.net/community-downloads"OpenVPN server").

[Raspbian](https://www.raspbian.org) è un sistema operativo Linux derivato dalla famosissima distribuzione Debian appositamente per il single board computer [Raspberry Pi](https://www.raspberrypi.org).

Un web server nginx a bordo del Raspberry permette una facile gestione da un browser web.

All'avvio del Raspberry vengono create sulla sua porta di rete due interfacce predisposte alla gestione di altrettanti tunnel criptati con modalità di funzionamento TLS con autenticazione e scambio chiavi a 256 bit e certificati X.509.

Il primo tunnel è in modalità TAP (bridging - livello 2 ISO/OSI) e simula il collegamento ethernet alla rete locale, rendendo disponibile tutto il traffico broadcast, ovvero rendendo disponibili sul computer servizi come stampanti, network storage, condivisione file...

Il secondo è in modalità TUN (routing- livello 3 ISO/OSI), più efficiente, ma che rende necessario consoscere gli indirizzi IP della rete locale per poter accedere ai servizi. E' comunque l'unica modalità attualmente supportata dai client OpenVPN installabili sui device portatili (smartphones, tablet...).

La creazione dei tunnel è effettuata dal Raspberry Pi utilizzando il programma opensource [OpenVPN](https://openvpn.net) ([Wikipedia](https://it.wikipedia.org/wiki/OpenVPN)).

### Primo avvio - configurazione VPN (metodo semplice, partendo da immagine PBOX)

Scrivere l'immagine contenuta nella directory `/dist/PBOX.img` sulla sd card.

> *L'immagine è ricavata dal sistema operativo raspian buster lite del 13-02-2020 versione kernel 4.19 al quale sono stati aggiunti i pacchetti necessari al funzionamento della PBOX.*

Quindi inserirla nel raspberry collegato alla rete con un cavo ethernet (non WIFi, la WiFi del raspberry è inizialmente spenta) e attendere circa un minuto.

Con un browser collegarsi all'indirizzo http://pbox.local .

Al primo utilizzo è necessario confermare/inserire le seguenti informazioni:

| Parametro                     | Significato                                                  |
| :---------------------------- | ------------------------------------------------------------ |
| **IP Statico**                | E' l'indirizzo che deve avere la PBOX.<br />L'indirizzo proposto è quello che è stato assegnato dal server DHCP presente sulla rete.<br />*Deve essere un indirizzo permanente, e quindi dovrà essere modificato per essere tra quelli non assegnati dal DHCP oppure appartenere ad una lista di prenotazione.* |
| **Netmask**                   | La netmask della vostra rete locale. Anche questa è proposta secondo quanto fornito dal server DHCP e non dovrebbe essere necessario modificarla. |
| **Gateway**                   | L'indirizzo del router o del firewall tramite il quale si effettua l'accesso ad internet. |
| **DNS** primario e secondario | Gli indirizzi dei server DNS da utilizzare separati da spazio |
| **Indirizzo IP pubblico**     | E' l'indirizzo fornito dal provider alla connessione internet.<br />Se si tratta di un indirizzo assegnato dinamicamente, sarà necessario utilizzare un servizio di DNS dinamico compatibile con quelli gestiti dal router che effettua la connessione internet *(potrebbe essere necessario il pagamento di un canone mensile)*. |

Quindi controllare quanto inserito e procedere con l'installazione, durante la quale verranno generati i certificati univoci per questa PBOX e verrà opportunamente modificata la configurazione di rete secondo le informazioni inserite. Al termine dell'installazione la PBOX necessita un riavvio.

Dopo circa due minuti la PBOX è pronta e sarà disponibile nuovamente all'indirizzo http://pbox.local per poter generare i profili OpenVPN necessari ai Client per la connessione.

La password predefinita per l'accesso è <u>**pbox**</u> e si consiglia di cambiarla al primo utilizzo.

### Primo avvio - configurazione VPN (metodo più complicato, partendo da immagine raspbian)

Se non volete utilizzare l'immagine già preparata in `/dist/PBOX.img` è possibile installare il software necessario al funzionamento della PBOX partendo da un'immagine del sistema operativo Raspian Buster scaricato dal [sito ufficiale](https://www.raspberrypi.org/downloads/raspbian/ "sito ufficiale").

I passi da seguire sono indicati nel README nella directory `/src`.

------

### Configurazione del router

Il router necessita delle seguenti configurazioni:

1. Impostare regola di forward della porta 1194 UDP verso l'indirizzo specificto nel parametro **IP Statico**
2. inpostare regola di forward della porta 1418 UDP verso l'indirizzo specificto nel parametro **IP Statico**
3. se nel parametro **Indirizzo IP pubblico** è stato inserito un indirzzo di un DNS dinamico (es: mionome.ddns.net), il router dovrà essere configurato per aggiornare automaticamente il servizio relativo al DNS dinamico utilizzato.

------

### Generazione di un profilo OpenVPN per la connessione

I profili vengono generati utilizzando l'interfaccia web della PBOX all'indirizzo http://pbox.local (oppure http://ip_statico).

Se si è eseguita il primo avvio la schermata mostra:

- l'elenco dei profili generati con i pulsanti per il download dei file OpenVPN per i client e il pulsante di revoca
- Un pulsante per la generazione di un nuovo profilo di connessione
- la situazione dei collegamenti VPN attivi al momento

Premendo il pulsante per la generazione di un nuovo profilo viene mostrata una dialog che chiede le seguenti informazioni relative al profilo da generare:

1. Nome del profilo
2. Durata del certificato (in giorni)
3. Password da fornire al momento del collegamento (opzionale - se non viene immessa il collegamento non necessiterà dell'immissione di una password per essere stabilito).

Quindi verrà generato un nuovo profilo e la pagina principale verrà aggiornata mostrandolo e quindi rendendo possibile il download del file *.ovpn* necessario per la connessione del client.

### Metodologie dei profili di connessione (*desktop* o *mobile*)

##### Profilo Desktop - bridge mode 

Premendo sul pulsante di download verrà scaricato un file `nomeprofilo.ovpn` che può essere installato nel software OpenVPN client del proprio dispositivo.

Questo profilo attiverà una connessione VPN bridged in *TAP o bridge mode*.

> ATTENZIONE: non tutti i client OpenVPN supportano la modalità TAP e al momento questa non è supportata da alcun client OpenVPN installabile su smartphone e tablet (sistemi iOS e Android).

E' il profilo normalmente da utilizzare in una connessione con un computer desktop, in quanto permette di accedere immediatamente a tutte le condivisioni presenti nella rete a cui ci si collega, ricevendo e partecipando al traffico broadcast.

Sarà quindi possibile accedere a stampanti, server e condivisioni vedendoli apparire nelle ceonnessioni di rete.

##### Profilo Mobile - routing mode

Premendo sul pulsante di download verrà scaricato un file `nomeprofilo_mob.ovpn` che può quindi essere installato nel software OpenVPN client del proprio dispositivo.

Questo profilo attiverà una connessione VPN in *TUN o routing mode*.

> Tutti i client OpenVPN supportano la modalità TUN. Uno di questi è proprio il client OpenVPN originale [windows](https://openvpn.net/client-connect-vpn-for-windows/), [mac](https://openvpn.net/client-connect-vpn-for-mac-os/), [iOS](https://apps.apple.com/it/app/openvpn-connect/id590379981), [Andorid](https://play.google.com/store/apps/details?id=de.blinkt.openvpn&hl=it), oltre ai client consigliati nella sezione "Cosa vi serve". Sono tutti freeware.

E' il profilo normalmente da utilizzare sui cellulari poiché non su questi l'altra modalità non è supportata.

Nella modalità TUN è possibile accedere a tutti i dispositivi della rete locale a cui si viene connessi, ma per farlo è necessario conoscere il loro indirizzo IP.

------

### Perché la PBOX

E' una conseguenza del covid-19.

Durante il lockdown molti mi chiedevano come fare a lavorare da casa e accedere ai loro storage o server dell'ufficio.

Professionisti, piccole aziende che non si potevano permettere un costoso router e complicate procedure di configurazione.

Lo smart working (o lavoro agile) è nato, e avrà una lunga vita.

La PBOX è comunque utile anche per avere una casa domotica più sicura, con meno IoT che si collegano a server esterni per fornire gli accessi sul cellulare ai vari dispositivi. Con PBOX è come accederci da casa e anche la domotica può essere più sicura e sotto controllo.

### Riferimenti software

Il software è stato ricavato utilizzando parti rilevanti dei seguenti pacchetti opensource:

PiVPN Project - https://pivpn.io

Open Raspberry Pi Server Dashboard - https://github.com/mitchellurgero/openrsd

