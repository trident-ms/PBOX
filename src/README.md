# PBOX - La Private Box per lo smart working

Questa directory contiene il codice necessario per trasformare il Rasperry Pi in una PBOX partendo dall'immagine del sistem operatio Raspian buster Lite.

Il risultato è lo stesso che si otterrebbe utilizzando direttmente l'immagine fornita in /dist/PBOX.img.gz con l'unica differenza che in questo modo verranno installate le utlime versioni dei pacchetti necessari.

## CREAZIONE DELLA PBOX

### Come preparare l'immagine per la PBOX - PASSO 1

1. scaricare l'immagine del sistema raspbian buster lite dal [sito ufficiale](https://www.raspberrypi.org/downloads/raspbian/ "sito ufficiale")
2. scrivere l'immagine di raspbian buster lite sulla microSD card utilizzando balenaEtcher o altro card imager
3. Montare l'immagine sul computer e copiare il contenuto della directory `/src/boot` nella partizione /boot dell'immagine.
   **ATTENZIONE**: *copiare anche il file (vuoto) `/boot/ssh` per permettere l'apertura della porta ssh del raspberry al suo avvio.*
4. Smontare il disco immagine ed estrarre la microSD
5. inserire nel raspberry la microSD
6. collegare il raspberry alla rete ethernet
7. avviare il raspberry e attendere circa 1 min
8. collegarsi al raspberry (login pi/raspberry)
> il login può avvenire anche in SSH ottenendo l'indirizzo dall'interfaccia del proprio router
7. avviare il primo setup con il comando
```shell
sudo /boot/pbox/setup
```
> **ATTENZIONE**: <u>NON ESEGUIRE RASPI-CONFIG</u> - potrà comunque essere eseguito successivamente con le dovute accortezze nel non modificare in alcun modo i parametri di networking.

> ##### cosa fa il primo setup
>
> - setta il locale (it + en)
> - setta la timezone (it)
> - setta il country (italia)
> - effettua gli update del software raspbian lite (buster) all'ultima versione
> - installa pacchetti necessari non compresi in raspbian lite (buster) dai repository ufficiali
> - configura il web server nginx con php-fpm creando un utente "pboxwebuser" con password di default "pbox"
> - cambia la password dell'utente "pi"
>

8. attendere il completamento (saranno necessari alcuni minuti)
   - rispondere **NO** alle domande relative ad iptables (<u>attenzione NO non è il default!</u>)
   - inserire la nuova password dell'utente "pi"
   - eseguire il riavvio

------

A questo punto IL PBOX è pronto per eseguire il *"Primo Avvio"* con un browser all'indirizzo http://pbox.local

***locale, timezone, country e password possono essere cambiati anche successivamente tramite terminale con il comando `raspi-config`***

Con un browser collegarsi all'indirizzo http://pbox.local .

Al primo utilizzo è necessario confermare/inserire le seguenti informazioni:

| Parametro                     | Significato                                                  |
| :---------------------------- | ------------------------------------------------------------ |
| **IP Statico**                | E' l'indirizzo che deve avere la PBOX.<br />L'indirizzo proposto è quello che è stato assegnato dal server DHCP presente sulla rete.<br />*Deve essere un indirizzo permanente, e quindi dovrà essere modificato per essere tra quelli non assegnati dal DHCP oppure appartenere ad una lista di prenotazione.* |
| **Netmask**                   | La netmask della vostra rete locale. Anche questa è proposta secondo quanto fornito dal server DHCP e non dovrebbe essere necessario modificarla. |
| **Gateway**                   | L'indirizzo del router o del firewall tramite il quale si effettua l'accesso ad internet. |
| **DNS** primario e secondario | Gli indirizzi dei server DNS da utilizzare separati da spazio |
| **Indirizzo IP pubblico**     | E' l'indirizzo fornito dal provider alla connessione internet.<br />Se si tratta di un indirizzo assegnato dinamicamente, sarà necessario utilizzare un servizio di DNS dinamico compatibile con quelli gestiti dal router che effettua la connessione internet *(potrebbe essere necessario il pagamento di un canone mensile)*. |

Quindi controllare quanto inserito e procedere con l'installazione, durante la quale verranno generati i certificati univoci per questa PBOX e verrà opportunamente modificata la configurazione di rete secondo le informazioni inserite. Al termine dell'installazione la PBOX necessita un riavvio.

Dopo circa due minuti la PBOX è pronta e sarà disponibile nuovamente all'indirizzo http://pbox.local per poter generare i profili OpenVPN necessari ai Client per la connessione.

La password predefinita per l'accesso è <u>**pbox**</u> e si consiglia di cambiarla al primo utilizzo.

------

### Sorgenti html

La directory html è contenuta nel file `/boot/pbox/ngin/html_contents.tgz` che viene scompattata ed installata al momento del primo setup.

Il contenuto di `/boot/pbox/ngin/html_contents.tgz` è riportato nella directory `/src/var/www/html` per riferimento.