<?php
session_start();
if(isset($_POST['page'])){
	switch($_POST['page']){
		case "main":
			main();
			break;
		case "install":
			install();
			break;
		default:
			echo "404 - Page not found!";
			break;
		
	}
}

//Simple page functions..
function main() {
	// se al primo accesso (pivpn non installato) carica firstsetup
	if($_SESSION['firstrun'] || !file_exists('./app/pivpn/installed')) {
		include('pages/firstsetup.php');
	// altrimenti apri la pag per le ovpn
	} else {
		include('pages/openvpn.php');
	}
}
function install() {
	include('pages/installer.php');
}
?>
