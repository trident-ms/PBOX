<?php
//Check for valid session:
include('app/functions.php');
function CIDRtoMask($int) {
	return long2ip(-1 << (32 - (int)$int));
}
function alignedCIDR($ipinput,$netmask){
	return long2ip((ip2long($ipinput)) & (ip2long($netmask)));
}

UserSession();
$ipdata = getNetworkInfo();
$netmask = CIDRtoMask($ipdata['mask']);
$network = alignedCIDR($ipdata['ip'],$netmask);
$log_files = getDirContents('/home/pi/ovpns');
?>
	<div class="row">
		<div class="col-12">
			<h1 class="page-header">Profili di connessione VPN <small><a href="/" title="Click per aggiornare la pagina"><div onClick="pageLoad('PiVPN');" class="fa fa-refresh rotate"></div></a></small></h1>
			<table class="table">
				<tr>
					<th width="20%">Indirizzo locale PBOX</th>
					<th width="20%">Indirizzo Gateway</th>
					<th>Indirizzo pubblico connessione internet</th>
				</tr>
				<tr>
					<td><?= $ipdata['ip'] ?></td>
					<td><?= $ipdata['gw'] ?></td>
<?php if(count($log_files)!=0) { ?>
					<td><?= $ipdata['host'] ?> <a href="#" disabled class="m-0 btn btn-xs btn-primary btn-raised" title="Non è possibile cambiare indirizzo con profili attivi. Revocare tutti i profili.">cambia</a></td>
<?php } else { ?>
					<td><?= $ipdata['host'] ?> <a href="#" onclick="changeHost('<?= $ipdata['host'] ?>')" class="m-0 btn btn-xs btn-primary btn-raised">cambia</a></td>
<?php } ?>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table">
				<thead>
					<th>Profili VPN per accesso Client</th>
				</thead>
				<tbody>
					<tr>
						<th>Nome profilo</th>
						<th>DOWNLOAD OVPN CLIENT <small>per desktop</small></th>
						<th>DOWNLOAD OVPN CLIENT <small>per mobile</small></th>
						<th>REVOCA</th>
					</tr>
<?php
	foreach($log_files as $log) {
		$f = explode("/", $log);
		$file = end($f);
?>
					<tr>
						<td style="vertical-align: middle;">
							<h4><?= $file ?></h4>
						<td>
							<a href="dlnd_profile.php?filename=<?= urlencode($file) ?>" class="btn btn-sm btn-raised btn-info">Download OVPN desktop</a>
						</td>
						<td>
							<a href="dlnd_profile.php?filename=<?= urlencode($file) ?>&tun" class="btn btn-sm btn-raised btn-info">Download OVPN mobile</a>
						</td>
						<td>
							<button class="btn btn-sm btn-raised btn-warning" onclick="rProfile('<?= $file ?>')">Revoca Client</button>
						</td>
					</tr>
<?php
	}
?>
				</tbody>
			</table>
			<button class="btn btn-sm btn-raised btn-success" type="button" onclick="createProfile('<?= $network ?>','<?= $netmask ?>')">Crea nuovo Profilo VPN di accesso alla rete locale</button>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-lg-12">
			<table class="table">
				<thead>
					<th>Attività dei profili VPN</th>
				</thead>
			</table>
			<pre>
				<?= console_cc_clear(shell_exec("sudo cat /var/log/openvpn-status.log")) ?>
			</pre>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table">
				<thead>
					<th>Lista dei provili VPN generati</th>
				</thead>
			</table>
			<?php
				$profile_stats = shell_exec("pivpn list");
				echo "<pre>".console_cc_clear($profile_stats)."</pre>";
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
