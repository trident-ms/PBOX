<?php
include('app/functions.php');
if (!isset($_SESSION)) {
	session_start();
}
$ipdata = getNetworkInfo();
$res=checkNetworkInfo(hash2formdata($ipdata));
$caninstall=$res['success'];
?>
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Personalizzazione prima installazione PBOX</h1>
            <p id="pagesubtit">Confermare o modificare i dati proposti, quindi salvarli e procedere con la preparazione personalizzata della PBOX.</p>
		</div>
	</div>
	<div id="ipdata">
		<table class="table">
			<tr>
				<th class="text-right" width="30%">IP Statico</th>
				<td><?= $ipdata['ip'] ?>/<?= $ipdata['mask'] ?></td>
			</tr>
			<tr>
				<th class="text-right" width="30%">Gateway</th>
				<td><?= $ipdata['gw'] ?></td>
			</tr>
			<tr>
				<th class="text-right" width="30%">DNS primario e secondario</th>
				<td><?= $ipdata['dns'] ?></td>
			</tr>
			<tr>
				<th class="text-right" width="30%">Indirizzo IP pubblico della connessione internet</th>
				<td><?= $ipdata['host'] ?></td>
			</tr>
		</table>	
		<div class="row">
			<div class="col-sm-6">
				<button class="btn btn-sm btn-raised btn-info" type="button" onclick='ModifyNetworkData(<?= json_encode($ipdata) ?>)'>Modifica i dati</button>
			</div>
	<?php if($caninstall) { ?>
			<div class="col-sm-6 text-right">
				<button class="btn btn-sm btn-raised btn-warning" type="button" onclick="do_install()">Procedi con l'installazione</button>
			</div>
	<?php } ?>
		</div>
	</div>
	<div id="reboot" class="col-sm-12 text-right hidden">
		<button class="btn btn-sm btn-raised btn-danger" type="button" onclick="power('reboot')">RIAVVIA LA PBOX</button>
    </div>
