<?php
$tun = isset($_GET['tun']);
@session_start();
if (isset($_SESSION['username']) && isset($_GET['filename'])) {
	$filename = $_GET['filename'];
	$outname = $tun ? preg_replace('/\.ovpn$/','_mob.ovpn',$filename):$filename;
	$fld = $tun ? 'ovpns_tun':'ovpns';
	$path = '/home/pi/'.$fld.'/'.$filename;
	if(file_exists($path)) {
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$outname.'"');
		header('Content-length: ' . filesize($path));
		readfile($path);
	}
} else {
	header("Location: /index.php");
}
?>