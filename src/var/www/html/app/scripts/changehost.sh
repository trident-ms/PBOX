#!/bin/bash
setupVars="/etc/pivpn/setupVars.conf"

if [ ! -f "${setupVars}" ]; then
	echo "::: Missing setup vars file!"
	exit 1
fi

source $setupVars

NEWHOST=$1
sed -i -e "s/pivpnHOST=$pivpnHOST/pivpnHOST=$NEWHOST/" $setupVars
sed -i -e "s/$pivpnHOST/$NEWHOST/" /etc/openvpn/easy-rsa/pki/Default.txt
echo "::: indirizzo modificato."
exit 0