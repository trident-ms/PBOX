<?php
//Check for valid session:
session_start();
include('functions.php');

function revoke_vpn_profile($profile) {
	$log = './tmp/revoke_' . md5($profile . time());
	$filepath = '/home/pi/ovpns/'.$profile;
	if(!file_exists($filepath)) return ('Il profilo OVPN non esiste!');
	if(preg_match('/(.*)\.ovpn$/',$profile,$parts)) {
		$pname = $parts[1];
		$output = shell_exec('sudo pboxvpn -r '.$pname.' 2>&1 | tee -a '.$log.' 2>/dev/null >/dev/null');
		$output .= "Revoco profilo $profile:\n";
		$output .= file_get_contents($log);
		unlink($log);
	} else $output="Il nome del profilo OVPN non è valido!'";
	return console_cc_clear($output);
}

UserSession();
if(!isset($_POST['profile'])){
	ErrorDie("Nessun profilo VPN selezionato!");
}
$pro = $_POST['profile'];
// $pro = str_replace("/home/pi/ovpns/","", $pro);
// $pro = str_replace(".ovpn","", $pro);
echo revoke_vpn_profile($pro);
?>
