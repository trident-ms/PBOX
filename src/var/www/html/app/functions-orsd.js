//uptime = window.setInterval(function(){status("uptime")}, 60000);
function failErrorModal(e,code,err) {
	load(false);
	if(e.responseText) {
		genModal("Errore", e.responseText);
	} else {
		genModal("Errore", err);
	}
}
function genErrorModal($tit,txt) {
	load(false);
	genModal($tit, txt);
}
function pageLoad(page){
 	document.title = "Loading..."
 	document.getElementById("pageContent").innerHTML = "<p>Loading " + page + ", Please wait...</p>";
 	load(true);
 	$.ajax({
		method:'post',
		url:'./page.php',
		data:{
			page:page
		},
		success:function(result) {
			document.getElementById("pageContent").innerHTML = result;
			document.title = capitalizeFirstLetter(page);
			load(false);
		}
	}).fail(function(e,code,err) {
		var $m="Il caricamento della pagina è fallito. Si prega di riprovare.";
		document.getElementById("pageContent").innerHTML = $m;
		genErrorModal('Errore',$m);
	});
}
function load(type){
	if(type === true){
		$("#coverlay").show();
		document.getElementById("loadAnim").style.display = '';
	} else {
		$("#coverlay").hide();
		document.getElementById("loadAnim").style.display = 'none';
	}
}
function PIalert() {
	genModal("Errore?", "Sembra che non riesca a contattare la PBOX - Potrebbe essere in riavvio, arrestata o potrebbe avere l'interruttore spento.");
}
function genModal(head, body, footBtn, closable=true){
	if(closable) {
		$('#genModalClose').removeClass('hidden');
	} else {
		$('#genModalClose').addClass('hidden');
	}
	document.getElementById("genModalHeader").innerHTML = head;
	document.getElementById("genModalBody").innerHTML = body;
	if(footBtn) {
		$('#genModalFooter').html(footBtn);
	} else {
		$('#genModalFooter').html('<button type="button" class="btn btn-sm btn-raised btn-primary" data-dismiss="modal">Chiudi</button>');
	}
	$("#genModal").modal('show');
}
function genModalUnclosable() {
	$('#genModalClose').addClass('hidden');
}
//========== utilities
function sleep(milliseconds) {
	const date = Date.now();
	let currentDate = null;
	do {
		currentDate = Date.now();
	} while (currentDate - date < milliseconds);
}
function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
function runScript(filename){
	load(true)
	$.ajax({
		method:'post',
		url:'./app/apps.php',
		data:{
			script:filename,
			type:'run'
		},
		success:function(result) {
			load(false);
			genModal("Script \"" + filename + "\" esito esecuzione:", "<pre>" + result + "</pre>");
			
		}
	}).fail(function(e,code,err) {
		failErrorModal(e,code,err);
	});
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function scrollToBottom_frame() {
	var	$fwin = document.getElementById('shellinaboxframe').contentWindow,
		$fh = $($fwin.document.getElementsByTagName('html')).height();
	$fwin.scrollTo(0,$fh);
}
//========== actions from modals
function oFactory() {	// from reset to factory modal
	if(!confirm("Sei sicuro?\nOgni personalizzazione effettuata verrà cancellata assieme agli eventuali profili esistenti.")) {
		return;
	}
	genModalUnclosable();
	$('#message').replaceWith('<iframe id="shellinaboxframe" src="/pages/factory.php">Your browser does not support inline frames.</iframe>')
	$('#btn').html('<p class="text-warning">Inizializzazione in corso. Attendere...</p>');

	load(true)
	$.ajax({
		method:'get',
		url:'./pages/factory.php',
		data:{},
		success:function(result) {
			$('#btn').html('<button type="button" class="btn btn-sm btn-raised btn-primary" data-dismiss="modal">Chiudi</button>');
			sleep(2000);
			load(false);
			pageLoad('main');
		}
	}).fail(function(e,code,err) {
		failErrorModal(e, code, err)
	});
}
function oProfile(){		// from create profile modal
	load(true)
	pname = document.getElementById("pname").value;
	days = document.getElementById("exdays").value;
	pw = document.getElementById("pw").value;
	pw2 = document.getElementById("pw2").value;
	nw = document.getElementById("network").value;
	nm = document.getElementById("netmask").value;
	$.ajax({
		method:'post',
		url:'./app/profile.php',
		data:{
			profile:pname,
			days:days,
			pw:pw,
			pw2:pw2,
			network:nw,
			netmask:nm
		},
		success:function(result) {
			load(false);
			genModal("Esito creazione profilo (" + pname + "):", '<pre style="overscroll-y:scroll; max-height:400px;">' + result + "</pre>");
			pageLoad('main');
		}
	}).fail(function(e,code,err) {
		failErrorModal(e, code, err)
	});
}
function oChangeHost(){		// from change host modal
	load(true);
	var oldHost = document.getElementById("oldHost").value;
	var newHost = document.getElementById("newHost").value;
	if(oldHost != newHost){
		$.ajax({
			method:'post',
			url:'./app/changehost.php',
			data:{
				host:newHost
			},
			success:function(result) {
				genModal("Esito:", "<pre>" + result + "</pre>");
				load(false);
				pageLoad('main');
			}
			}).failErrorModal;
	} else {
		genModal("Attenzione", "L'indirizzo non è cambiato!");
		load(false);
	}
}
function changePasswd(){		// from change password modal
	load(true);
	var oldpassword = document.getElementById("oldPasswd").value;
	var password = document.getElementById("newPasswd3").value;
	var password2 = document.getElementById("newPasswd4").value;
	if(password == password2){
		$.ajax({
			method:'post',
			url:'./app/users.php',
			data:{
				type:'change',
				username:'pboxwebuser',
				oldpass: oldpassword,
				password:password
			},
			success:function(result) {
				genModal("Esito:", "<pre>" + result + "</pre>");
				load(false);
				pageLoad('main');
			}
			}).failErrorModal;
	} else {
		genModal("Errore", "Le password non coincidono!");
		load(false);
	}
}
function modNetwork(formdata){	// from modify network modal
	load(true)
	$.ajax({
		method:'post',
		url:'./app/modNetwork.php',
		data:{
			data:formdata
		},
		success:function(result) {
			load(false);
			if(result.success) {
				genModal("Modifica dati di rete",result.msg);
				pageLoad('main');
			} else {
				genModal("Errore", result.msg);
			}
		}
	}).failErrorModal;
}

//========== actions from menus (click on a tags)
function power(type){
	$.ajax({
		method:'post',
		url:'./app/power.php',
		data:{
			power:type
		},
		success:function(result) {
			document.getElementById("pageContent").innerHTML = result;
		}
	}).fail(function(e) {
			document.getElementById("pageContent").innerHTML = "Sembra che non riesca a contattare la PBOX - Potrebbe essere in riavvio, arrestata o potrebbe avere l'interruttore spento.";
			PIalert();
	});
	return false;
}
function factory() {
	var contents='\
<div class="row">\
	<div class="col-sm-12">\
		<div id="message">\
			<h2 class="text-danger">ATTENZIONE</h2>\
			<p class="text-danger">La PBOX sarà completamente reinizializzata!</p>\
			<p>Ogni profilo di connessione verrà cancellato come anche le attuali chiavi di criptazione.</p>\
			<p></p>\
			<p>Dopo il reset della PBOX sarà possibile introdurre nuovi indirizzi di rete e gateway.</p>\
		</div>\
	</div>\
</div>\
';
	var footBtn='<div id="btn"><button class="btn btn-sm btn-raised btn-danger pull-right" type="button" onclick="oFactory()">INIZIALIZZA</button></div>';
	genModal("Inizializzazione PBOX di fabbrica", contents, footBtn);
	return false;
}
function changePwd(){
	var chgPswdForm='<form method="post" action="#">\
<div class="form-group label-floating is-empty" >\
	<label class="control-label" for="newPasswd3">Password attuale</label>\
	<input class="form-control" id="oldPasswd" required type="password">\
	<span class="material-input"></span>\
</div>\
<div class="form-group label-floating is-empty" >\
	<label class="control-label" for="newPasswd3">Nuova password</label>\
	<input class="form-control" id="newPasswd3" required type="password">\
	<span class="material-input"></span>\
</div>\
<div class="form-group label-floating is-empty" >\
	<label class="control-label" for="newPasswd4">Nuova password (ripetere)</label>\
	<input class="form-control" id="newPasswd4" required type="password">\
	<span class="material-input"></span>\
</div>\
</form>';
	var footBtn='<button type="button" class="btn btn-sm btn-raised btn-primary pull-right" type="button" onclick="changePasswd()">Cambia Password</button>';
	genModal("Cambio password di accesso", chgPswdForm, footBtn);
	return false;
}

//========== actions from buttons
function rProfile(user){
	load(true)
	$.ajax({
		method:'post',
		url:'./app/profile-revoke.php',
		data:{
			profile:user
		},
		success:function(result) {
			load(false);
			genModal("Esito revoca profilo (" + user + "):", '<pre style="overscroll-y:scroll; max-height:400px;">' + result + "</pre>");
			pageLoad('main');
		}
	}).failErrorModal;
}
function createProfile(network,netmask){
	var profileForm='<form method="post" action="#">\
<div class="row">\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="pname">Nome del profilo</label>\
			<input class="form-control" type="text" placeholder="Nome del profilo che verrà generato" value="" name="pname" id="pname">\
		</div>\
	</div>\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="exdays">Durata in giorni della validità</label>\
			<input class="form-control" type="text" placeholder="Numero di giorni di validità dell\'accesso" name="exdays" id="exdays">\
		</div>\
	</div>\
</div>\
<div class="row">\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="pw">Password (opzionale)</label>\
			<input class="form-control" type="password" placeholder="Lasciare vuoto per accesso senza password" name="pw" id="pw">\
		</div>\
	</div>\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="pw2">Ripeti password</label>\
			<input class="form-control" type="password" name="pw2" id="pw2">\
		</div>\
	</div>\
</div>\
<input type="hidden" name="network" id="network" value="'+network+'" />\
<input type="hidden" name="netmask" id="netmask" value="'+netmask+'" />\
</form>';
	var footBtn='<button class="btn btn-sm btn-raised btn-info pull-right" type="button" onclick="oProfile()">Crea il profilo</button>';
	genModal("Creazione nuovo profilo VPN", profileForm, footBtn);
}
function changeHost(currHost){
	var chgHostForm='<form method="post" action="#">\
<div class="row">\
	<div class="col-lg-12">\
		<div class="form-group">\
			<label for="newHost">Indirizzo IP pubblico della connessione internet</label>\
			<input class="form-control" type="text "pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$|^[a-z0-9_-]*\.[a-zA-Z]*\.[a-zA-Z]*$" placeholder="MANCANTE - Esempio: 212.216.223.78 oppure mionome.ddns.net" name="newHost" value="'+currHost+'" id="newHost">\
		</div>\
	</div>\
</div>\
<input type="hidden" name="oldHost" id="oldHost" value="'+currHost+'" />\
</form>';
	var footBtn='<button type="button" class="btn btn-sm btn-raised btn-primary pull-right" type="button" onclick="oChangeHost(\''+currHost+'\')">Cambia Host</button>';
	genModal("Cambio indirizzo host", chgHostForm, footBtn);
}
function ModifyNetworkData(ipdata) {
	var profileForm='';
	profileForm = '<form id="netform" method="post" action="#">\
<div class="row">\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="f0">IP Statico</label>\
			<input class="form-control" type="text" pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$" placeholder="MANCANTE - Esempio: 192.168.1.253" value="'+ipdata.ip+'" name="ip" id="f0">\
		</div>\
	</div>\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="f1">Netmask</label>\
			<input class="form-control" type="text" pattern="^[0-9]{1,2}$" placeholder="MANCANTE - Esempio: 24" value="'+ipdata.mask+'" name="mask" id="f1">\
		</div>\
	</div>\
</div>\
<div class="row">\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="f2">Gateway</label>\
			<input class="form-control" type="text" pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"  placeholder="MANCANTE - Esempio: 192.168.1.254" value="'+ipdata.gw+'" name="gw" id="f2">\
		</div>\
	</div>\
	<div class="col-sm-6">\
		<div class="form-group">\
			<label for="f3">DNS primario e secondario</label>\
			<input class="form-control" type="text" pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\\s[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})?$"  placeholder="MANCANTE - Esempio: 192.168.102.253 208.67.222.222" value="'+ipdata.dns+'" name="dns" id="f3">\
		</div>\
	</div>\
</div>\
<div class="row">\
	<div class="col-lg-12">\
		<div class="form-group">\
			<label for="f4">Indirizzo IP pubblico della connessione internet</label>\
			<input class="form-control" type="text "pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$|^[a-z0-9_-]*\.[a-zA-Z]*\.[a-zA-Z]*$" placeholder="MANCANTE - Esempio: 212.216.223.78 oppure mionome.ddns.net" name="host" value="'+ipdata.host+'" id="f4">\
		</div>\
	</div>\
</div>\
</form>';
	var footBtn='<button class="btn btn-sm btn-raised btn-info pull-right" type="button" onclick="modNetwork($(\'#netform\').serializeArray())">Memorizza i nuovi dati</button>';
	genModal("Modifica i parametri di rete della PBOX", profileForm, footBtn);
}
//======== DOM utilities
function do_install() {	// called by install modal
	$('#ipdata').replaceWith('<iframe id="shellinaboxframe" src="/pages/installer.php">Your browser does not support inline frames.</iframe>')
	$('#pagesubtit').html('Personalizzazione della PBOX in corso...');
}

function install_success() {		// called by install modal
	$('#pagesubtit').html('<span class="text-success">Personalizzazione terminata.</span><br /><small>Osservare il log e effettuare il riavvio del dispositivo premendo il pulsante.</small>');
	$('#reboot').removeClass('hidden');

}
function install_fail() {	// called by install modal
	$('#pagesubtit').html('<span class="text-danger">Personalizzazione terminata con errore.</span><br /><small>Osservare il log.</small>');
}
