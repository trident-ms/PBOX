<?php
function getRam(){
	$total = exec("grep MemTotal /proc/meminfo | awk '{print $2}'");
	$free = exec("grep MemFree /proc/meminfo | awk '{print $2}'");
	$cached = shell_exec("grep Cached /proc/meminfo | awk '{print $2}'");
	$used = $total - $free - $cached;
	$free = $free + $cached;
	$total = round($total / 1024, 2)."MB";
	$free = round($free / 1024, 2)."MB";
	$used = round($used / 1024, 2)."MB";
//	if($return == "total"){
//		return $total;
//	} elseif($return == "free"){
//		return $free;
//	} elseif($return == "used"){
//		return $used;
//	} else {
//		return "Bad Arguments!";
//	}
	return "Total: $total<br />Used: $used <br />Free: $free";
}
function getCPUTemp(){
	$temp = exec("cat /sys/class/thermal/thermal_zone0/temp");
	$temp2 = $temp / 1000;
	echo $temp2."'C";
}
function getUptime(){
    $file = @fopen('/proc/uptime', 'r');
    if (!$file) return 'Opening of /proc/uptime failed!';
    $data = @fread($file, 128);
    if ($data === false) return 'fread() failed on /proc/uptime!';
    $upsecs = (int)substr($data, 0, strpos($data, ' '));
    $uptime = Array (
        'days' => floor($data/60/60/24),
        'hours' => $data/60/60%24,
        'minutes' => $data/60%60,
        'seconds' => $data%60
    );
    return "Uptime: ".$uptime["days"]."D ".$uptime["hours"]."H ".$uptime["minutes"]."M";
}
function getCurrentIP($name){
	return exec("/sbin/ifconfig $name | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'");
}
function getCPU(){
	$speed = 1;
    //Sets variable with current CPU information and then turns it into an array seperating each word.
    $prevVal = shell_exec("cat /proc/stat");
    $prevArr = explode(' ',trim($prevVal));
    //Gets some values from the array and stores them.
    $prevTotal = $prevArr[2] + $prevArr[3] + $prevArr[4] + $prevArr[5];
    $prevIdle = $prevArr[5];
    //Wait a period of time until taking the readings again to compare with previous readings.
    usleep($speed * 1000000);
    //Does the same as before.
    $val = shell_exec("cat /proc/stat");
    $arr = explode(' ',trim($val));
    //Same as before.
    $total = $arr[2] + $arr[3] + $arr[4] + $arr[5];
    $idle = $arr[5];
    //Does some calculations now to work out what percentage of time the CPU has been in use over the given time period.
    $intervalTotal = intval($total - $prevTotal);
    //Does a few more calculations and outputs total CPU usage as an integer.
    return intval(100 * (($intervalTotal - ($idle - $prevIdle)) / $intervalTotal));
	//return  "CPU Usage:".exec("top -b -n1 | grep \"Cpu(s)\" | awk '{print $2 + $4}'")."%";
}
function generateString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getUpdates(){
	$isThere = shell_exec("./scripts/updates.sh");
	$number = substr_count( $isThere, PHP_EOL );
	$number = $number - 1;
	if($number <= 0){
		return "No updates found";
	} else {
		return $number.' <a href="#" data-toggle="modal" data-target="#checkUpdates">Updates available.</a>';
	}

}
function writeFileA($file, $content){
	$myfile = fopen($file, "a") or die("Unable to open file!");
	fwrite($myfile, $content."\n");
	fclose($myfile);
	return true;
}

function writeFileC($file, $content){
	$myfile = fopen($file, "w") or die("Unable to open file!");
	fwrite($myfile, $content) or die(false);
	fclose($myfile);
	return true;
}
function readFileAll($file){
	$fileContent = file_get_contents($file);
	return $fileContent;
}
function getDirContents($dir, &$results = array()){
    $files = @scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        $dno = false;
        $ext = array(".gz", ".zip");
        foreach($ext as $t){
        	if(substr($value, -3) == $t or substr($value, -4) == $t){ $dno = true; }
        }
        if(!is_dir($path)) {
        	if($dno != true){
        		$results[] = $path;
        	}
        } else if($value != "." && $value != "..") {
            getDirContents($path, $results);
            //$results[] = $path;
        }
    }

    return $results;
}
function hash2formdata($data) {
	$out = Array();
	foreach($data as $n => $v) {
		$out[] = Array('name' => $n, 'value' => $v);
	}
	return $out;
}
function formdata2hash($hash) {
	$out = Array();
	foreach($hash as $v) {
		$out[$v['name']] = $v['value'];
	}
	return $out;
}
function getCurrentNetworkInfo() {
	list($addr,$mask) = explode('/',exec("ip -o -f inet address show dev eth0 | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,2}'"));
	$gw = exec("ip -o route get 192.0.2.1 | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | awk 'NR==2'");
	$dns = exec("grep -v \"^#\" /etc/resolv.conf | grep -w nameserver | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | xargs");
	$dns = implode(' ',array_slice(explode(' ',$dns),0,2));	// prendi solo i primi 2

	return Array(
		'ip' 	=> $addr,
		'mask'	=> $mask,
		'gw'		=> $gw,
		'dns'	=> $dns,
		'host'	=> ''
	);
}
function getNetworkInfo() {
	$f = __DIR__.'/tmp/networkdata';
	if(!file_exists($f)) {
		file_put_contents($f,serialize(getCurrentNetworkInfo()));
	}
	return unserialize(file_get_contents($f));
}
function checkNetworkInfo($data,$checkhost=true,$checkalldata=true) {
	$vars = $msgs = Array();
	$numvals = 0;
	$ok=true;
	foreach($data as $el) {
		switch($el['name']) {
			case 'ip':
				$val = trim($el['value']);
				if(empty($val) || !filter_var($val, FILTER_VALIDATE_IP)) {
					$ok=false;
					$msgs[]='L\'indirizzo IP non è valido';
				}	
				break;
			case 'mask':
				$val = trim($el['value']);
				if(!preg_match('/^[0-9]{1,2}$/',$val)) {
					$ok=false;
					$msgs[]="La netmask '$val' non è valida";
				}
				break;
			case 'gw':
				$val = trim($el['value']);
				if(empty($val) || !filter_var($val, FILTER_VALIDATE_IP)) {
					$ok=false;
					$msgs[]="L'indirizzo del gateway '$val' non è valido";
				}
				break;
			case 'dns':
				$vals = explode(' ',trim($el['value']),2);
				$vv=Array();
				foreach($vals as $k => $v) {
					$vv[$k] = trim($v);
					if(empty($val) || !filter_var($vv[$k], FILTER_VALIDATE_IP)) {
						$ok=false;
						$msgs[]="L'indirizzo del DNS '".$vv[$k]."' non è valido";
					}
				}
				$val = implode(' ',$vv);
				break;
			case 'host':
				$val = trim($el['value']);
				if($checkhost) {
					$err=true;
					if(empty($val)) {
						$m = "L'indirizzo dell'host non può essere vuoto.";
					} else {
						$m = "L'indirizzo dell'host '".$val."' non è valido";
						if(filter_var($val, FILTER_VALIDATE_IP)) {
							$err=false;
						} else {
							if(preg_match('/^[a-zA-Z0-9_-]*\.[a-zA-Z0-9_-]*\.[a-zA-Z0-9_-]*$/',$val)) $err=false;
						}
					}
					if($err) {
						$ok=false;
						$msgs[]=$m;
					}
				}
				break;
		}	// end switch
		$vars[$el['name']] = $val;
	}	// end foreach

	$alldata = $checkalldata ? isset($vars['ip']) && isset($vars['mask']) && isset($vars['gw']) && isset($vars['dns']) : true;
	if($checkhost) $alldata = $alldata && isset($vars['host']);
	if(!$alldata) $msgs[]='Dati mancanti.';
	return Array(
		'success'	=> $ok && $alldata,
		'msg'		=> implode("<br />",$msgs),
		'vars'		=> $vars
	);
}
function storeNetworkInfo($data,$checkhost=true) {
	$res = checkNetworkInfo($data,$checkhost);
	if($res['success']) {
		$f = __DIR__.'/tmp/networkdata';
		if(file_put_contents($f,serialize($res['vars']))===false) {
			$res['success']=false;
			$res['msg']='Errore in salvataggio informazioni su disco.';
		} else {
			$res['msg'] = 'Dati salvati correttamente.';
			$f2 =  __DIR__.'/pivpn/setup.conf';
			$ip = $res['vars']['ip'];
			$mask = $res['vars']['mask'];
			$gw = $res['vars']['gw'];
			$host = $res['vars']['host'];
			list($dns1,$dns2) = explode(' ',$res['vars']['dns']);
			$conts = <<<EOT
PLAT=Raspbian
OSCN=buster
USING_UFW=0
IPv4dev=eth0
IPv4addr=$ip/$mask
IPv4gw=$gw
install_user=pi
install_home=/home/pi
VPN=openvpn
pivpnPROTO=udp
pivpnPORT=1194
pivpnDNS1=$dns1
pivpnDNS2=$dns2
pivpnSEARCHDOMAIN=
pivpnHOST=$host
TWO_POINT_FOUR=1
pivpnENCRYPT=256
USE_PREDEFINED_DH_PARAM=0
INPUT_CHAIN_EDITED=0
FORWARD_CHAIN_EDITED=0
pivpnDEV=tun0
pivpnNET=10.8.0.0
subnetClass=24
UNATTUPG=0
INSTALLED_PACKAGES=(git dnsutils iptables-persistent openvpn grepcidr expect)
HELP_SHOWN=1
EOT;
			if(file_put_contents($f2,$conts)===false) {
				$res['success']=false;
				$res['msg']='Errore in salvataggio informazioni su disco. (2)';
			}
		};
	}
	return($res);
}
function ErrorDie($s,$code=500) {
	header($_SERVER['SERVER_PROTOCOL'] . " $code Internal Server Error", true, $code);
	echo $s;
	exit(1);
}
function UserSession() {
	if (!isset($_SESSION)) {
		session_start();
	}
	if(!isset($_SESSION['username'])){
		ErrorDie("Non sei abilitato alla visione della pagina.");
	}
}
function console_cc_clear($s) {
	return preg_replace('/\x1B\x5B.{2}/','', $s);
}
?>