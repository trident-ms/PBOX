<?php
//Check for valid session:
include('functions.php');
UserSession();
$netdata = getNetworkInfo();

$newhost = trim($_POST['host']);
if($newhost == $netdata['host']) {
	ErrorDie("L'indirizzo non è cambiato!");
}
$netdata['host'] = $newhost;
$res = checkNetworkInfo(Array(Array('name' => 'host', 'value' => $newhost)),true,false);
if(!$res['success']) {
	ErrorDie($res['msg']);
}
$cmdpath = realpath(__DIR__ . '/scripts/changehost.sh');

$log = './tmp/chost_' . md5($newhost . time());
$cmd = "sudo $cmdpath $newhost";
$output = shell_exec($cmd.' 2>&1 | tee -a '.$log.' 2>/dev/null >/dev/null');
$output .= "Cambio host $newhost:\n";
$output .= file_get_contents($log);
unlink($log);
if(preg_match('/::: indirizzo modificato/',$output)) {
	$f = realpath(__DIR__.'/tmp/networkdata');
	file_put_contents($f,serialize($netdata));
	$output .= "\nDati aggiornati.";
}
echo console_cc_clear($output);
?>
