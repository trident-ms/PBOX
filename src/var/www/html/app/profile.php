<?php
//Check for valid session:
include('functions.php');

function add_vpn_profile($profile, $d, $network, $netmask, $pw='') {
	$log = './tmp/add_' . md5($profile . time());
	$cmd = 'sudo pboxvpn -a -n '.$profile.' -d '.$d.' -w '.$network.' -m '.$netmask;
	if(empty($pw)) {
		$pwdstr = "senza password di accesso";
		$cmd .= ' nopass';
	} else {
		$pwdstr = "con password di accesso";
		$cmd .= ' -p '.$pw;
	}
	$output = shell_exec($cmd.' 2>&1 | tee -a '.$log.' 2>/dev/null >/dev/null');
	$output .= "Genero profilo $profile $pwdstr:\n";
	$output .= file_get_contents($log);
	unlink($log);
	return console_cc_clear($output);
}

UserSession();

$pro = trim($_POST['profile']);
if(empty($pro)){
	ErrorDie("Il nome del profilo è obbligatorio!");
}
$days = 1080; // Days is set with a default prompt value.
if (isset($_POST['days'])) {
	$days = intval($_POST['days']);
	if($days==0) {
		ErrorDie("La durata non può essere 0!");
	}
}
if(isset($_POST['pw'])) {
	if($_POST['pw'] !== $_POST['pw2']) {
		ErrorDie("Le password non corrispondono!");
	}
	$days = $_POST['days'];
	echo add_vpn_profile($pro, $days, $_POST['network'], $_POST['netmask'] , $_POST['pw']);
} else {
	echo add_vpn_profile($pro, $days, $_POST['network'], $_POST['netmask']);
}
?>
