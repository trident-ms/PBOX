<?php
//Check for valid session:
if (basename(dirname(__FILE__)) == 'app') {
    require_once('functions.php');
};
UserSession();

if (!isset($_POST['username'], $_POST['password'], $_POST['type'])) {
    die("Parametri mancanti");
}

if (isset($_POST['username'])) {
    $username = escapeshellcmd(escapeshellarg($_POST['username']));
}
if (isset($_POST['password'])) {
	$oldpass = escapeshellcmd(escapeshellarg($_POST['oldpass']));
    $password = escapeshellcmd($_POST['password']);
}
$type = $_POST['type'];

switch ($type) {
    case "change":
        changePass($username, $oldpass, $password);
        break;
    default:
        die("Errore");
        break;
}

function changePass($u, $o, $p) {
    $result = change_password($u, $o, $p);
    switch($result) {
    		case 0:
    			echo "Password cambiata.";
    			break;
    		case -1:
    			echo "Errore nella procedura. La password non è cambiata. Si prega di riprovare.";
    			break;
    		case -2:
    			echo "Password errata. Riprovare.";
    			break;
    }
}
function change_password($user, $oldpass, $newpwd) {
	$result = exec("sudo ./bin/chkpasswd $user $oldpass");
	if($result!='Authenticated') {
		return -2;
	}
    // Open a handle to expect in write mode
    $p = popen('sudo /usr/bin/expect', 'w');

    // Log conversation for verification
    $log = './tmp/passwd_' . md5($user . time());
    $cmd .= "log_file -a \"$log\"; ";

    // Spawn a shell as $user
    $cmd .= "spawn /bin/bash; ";
    //$cmd .= "expect \"Password:\"; ";
    //$cmd .= "send \"$currpwd\\r\"; ";
    //$cmd .= "expect \"$user@\"; ";

    // Change the unix password
    $cmd .= "send \"sudo /usr/bin/passwd $user\\r\"; ";
    //$cmd .= "expect \"(current) UNIX password:\"; ";
    //$cmd .= "send \"$currpwd\\r\"; ";
    $cmd .= "expect \"New password: \"; ";
    $cmd .= "send \"$newpwd\\r\"; ";
    $cmd .= "expect \"Retype new password: \"; ";
    $cmd .= "send \"$newpwd\\r\"; ";
    $cmd .= "expect \"passwd: password updated successfully\"; ";
    // Commit the command to expect & close
    fwrite($p, $cmd);
    pclose($p);

    // Read & delete the log
    $fp = fopen($log, r);
    $output = fread($fp, 2048);
    fclose($fp);
    unlink($log);
    // print "$output";
    $output = explode("\n", $output);
    return (trim($output[count($output)-2]) == 'passwd: password updated successfully') ? 0 : -1;
}